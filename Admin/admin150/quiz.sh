#!/usr/bin/env bash
printf 'What are our competitions called?\n1) EscapeCTF\n2) SomethingCTF\n3) GeoCTF\n4) RussianCTF\n'
read EVENT

case $EVENT in
     EscapeCTF|escapectf|1)
          echo "It's true!"
          ;;
     *)
          echo "Sorry, bro!"
          ;;
esac

if [[ $EVENT = "1" || $EVENT = "EscapeCTF" || $EVENT =  "escapectf" ]]
then

printf '\nWhat is RAID?\n1) Redundant array of independent (or inexpensive) disks\n2) Remove Alcohol Impaired Drivers\n3) Restitution And Inmate Development\n4) Risks Assumptions Issues and Decisions\n'
read RAID

case $RAID in
     1)
       echo "It's true!"
          ;;
     *)
       echo "Sorry, bro!"
          ;;
esac
fi


if [[ $RAID = "1" ]]
then
printf '\nWhat is the entry rwx?\n1) Secret code\n2) File permissions (read,write,execute)\n3) Vim hotkeys\n4) Programming language\n'
read ENTRY

case $ENTRY in
     2|permissions)
          echo "It's true!"
          ;;
     *)
          echo "Sorry, bro!"
          ;;
esac
fi


if [[ $ENTRY = "2" || $ENTRY = "permissions" ]]
then
printf '\nHow many kilobytes in 1 megabyte?\n1) 1024\n2) 8\n3) 4096\n4) 1000\n'
read MANY

case $MANY in
     4|1000)
          echo "It's true!"
          ;;
     *)
          echo "Sorry, bro!"
          ;;
esac
fi


if [[ $MANY = "4" || $ENTRY = "1000" ]]
then
printf '\nAs you might call one word: VirtualBox, Vmvare Player, KVM, Xen?\n1) Application software\n2) Hypervisor\n3) Malware\n4) Games\n'
read HYPER

case $HYPER in
     2|Hypervisor|hypervisor)
          echo "It's true!"
          ;;
     *)
          echo "Sorry, bro!"
          ;;
esac
fi


if [[ $HYPER = "2" || $HYPER = "Hypervisor" || $HYPER = "hypervisor" ]]
then
printf $'\nIs it true that the world\'s first programmer was a woman?\n1) True\n2) False\n'
read PROGRAMMER

case $PROGRAMMER in
     1|True|true)
          echo "It's true!"
          ;;
     *)
          echo "Sorry, bro!"
          ;;
esac
fi


if [[ $PROGRAMMER = "1" || $PROGRAMMER = "True" || $PROGRAMMER = "true" ]]
then
printf '\nIs it true that the term "debugging" was also introduced by a woman?\n1) True\n2) False\n'
read DEBUG

case $DEBUG in
     1|True|true)
          echo "It's true!"
          ;;
     *)
          echo "Sorry, bro!"
          ;;
esac
fi


if [[ $DEBUG = "1" || $DEBUG = "True" || $DEBUG = "true" ]]
then
printf '\nWho invented the first computer mechanical machine?\n1) Archimedes\n2) Isaac Newton\n3) Blaise Pascal\n4) Leonardo da Vinci\n'
read MACHINE

case $MACHINE in
     3|Pascal|pascal)
          echo "It's true!"
          ;;
     *)
          echo "Sorry, bro!"
          ;;
esac
fi


if [[ $MACHINE = "3" || $MACHINE = "Pascal" || $MACHINE = "pascal" ]]
then
printf '\nWho invented the computer mouse?\n1) Douglas Carl Engelbart\n2) John Vincent Atanasoff\n3) Herman Hollerith\n4) Charles Babbage\n'
read MOUSE

case $MOUSE in
     1|Engelbart|engelbart)
          echo "It's true!"
          ;;
     *)
          echo "Sorry, bro!"
          ;;
esac
fi


if [[ $MOUSE = "1" || $MOUSE = "Engelbart" || $MOUSE = "engelbart" ]]
then
printf '\nWhen was e-mail created?\n1) 1990\n2) 1965\n3) 1972\n4) 1985\n'
read MAIL

case $MAIL in
     2|1965)
          echo "It's true!"
          ;;
     *)
          echo "Sorry, bro!"
          ;;
esac
fi


if [[ $MAIL = "2" || $MAIL = "1965" ]]
then
printf '\nWho is the author of WWW technology?\n1) Douglas Carl Engelbart\n2) Herman Hollerith\n3) Bill Gates\n4) Sir Timothy John «Tim» Berners-Lee\n'
read WWW

case $WWW in
     4|Berners-Lee|Tim)
          echo "It's true!"
          ;;
     *)
          echo "Sorry, bro!"
          ;;
esac
fi


if [[ $WWW = "4" || $WWW = "Berners-Lee" || $WWW = "Tim" ]]
then
printf '\nWhich search engine was historically the first?\n1) Alta Vista\n2) Google\n3) Wandex\n4) Yahoo\n'
read ENGINE

case $ENGINE in
     3|Wandex|wandex)
          echo "It's true!"
          ;;
     *)
          echo "Sorry, bro!"
          ;;
esac
fi


if [[ $ENGINE = "3" || $ENGINE = "Wandex" || $ENGINE = "wandex" ]]
then
printf '\nWhat is the name of the technology of creating corporate information systems, based on Internet protocols, but without access to the global network?\n1) Intranet\n2) FTP\n3) Extranet\n4) Botnet\n'
read TECH

case $TECH in
     1|Intranet|intranet)
          echo "It's true!"
          ;;
     *)
          echo "Sorry, bro!"
          ;;
esac
fi

if [[ $TECH = "1" || $TECH = "Intranet" || $TECH = "intranet" ]]
then
printf '\nIn what year was the Department of Information Security established in the SSUGT?\n1) 2007\n2) 2017\n3) 2018\n4) 2010\n'
read DEPART

case $DEPART in
     2|2017)
          echo "It's true!"
          echo "Flag is escctf{Qu1z_50lv3R_M4573r}"
          ;;
     *)
          echo "Sorry, bro!"
          ;;
esac
fi


