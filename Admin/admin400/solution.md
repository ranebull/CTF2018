# Решение
Перед использованием нужно сбросить пароль. В строке приглашения мы видим надпись "Windows 17 Ubuntu Edition" и начинаем разбираться, какие файлы отвечают за эту надпись.  
При подключении по SSH можно увидить удивительную картинку... За показ информации о системе отвечают файлы /etc/issue, /etc/os-release, /etc/lsb_release и  
/etc/update-motd.d/00-header. В них можно увидеть части флага. Для проверки можно воспользоваться командой:
```bash
cat /etc/lsb-release /etc/os-release /etc/update-motd.d/00-header | grep -A 1 '# PART'
```

*Флаг: escctf{4M4z1n6_W1nd0w5_d3v3l0p3R}*
