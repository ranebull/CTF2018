# Решение
В текстовом файле обнаруживаем код, "написанный" на эзотерическом языке программирования, на что также указывает название таска. 
Гуглим онлайн обфускатор. В полученном тексте нас интересуют только числа в данной строке: 
-  s:='101 115 099 099 116 102 123 109 048 082 051 095 055 114 052 110 036 102 048 114 077 097 055 049 111 078 036 095 112 076 051 052 036 051 125';

Это ASCII представления различных символов. Декодируем и получаем заветный флаг.

*Флаг: escctf{m0R3_7r4n$f0rMa71oN$_pL34$3}*