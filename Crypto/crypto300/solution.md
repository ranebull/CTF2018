# Решение
Нам дан двумерный матричный штрихкод. Понять, что это `Aztec Code` можно из описания или из гугла (поиском аналогов QR).   
Декодируем, получаем строку вида `NmI0ZDNkNzU2ZDRkNTIzMTUyNWE2NTc0NjI2MzY1NzM1MzY0NGY0YzRmNjQ0ZTc2NTIyZjZhN2E2OTY3M2QzMDUxMmI1MzM3NGY2NDRlNzY1MjJmNmEzMDRmNjQ0ZTc2NTIyZjZhNzU2YzRhNTI3OTU0Njg1MzM3NjQ0OTUzNjQ1MzRiNjk3NjZmNTk2NTcxNjI2MjM1MzVh==`  

Из двух знаков равно на конце строки понимаем, что имеем дело с `base64`. 
Снова декодируем, получаем `6b4d3d756d4d5231525a65746263657353644f4c4f644e76522f6a7a69673d30512b53374f644e76522f6a304f644e76522f6a756c4a52795468533764495364534b69766f59657162623535a`.  

Из строки видно, что это `hex`. 
Декодируем, получая `kM=umMR1RZetbcesSdOLOdNvR/jzig=0Q+S7OdNvR/j0OdNvR/julJRyThS7dISdSKivoYeqbb55`.   

На этом этапе может возникнуть ряд сложностей ввиду незнания шифров. На помощь может прийти [статья](http://itsecwiki.org/index.php/String_crypto), либо иной подобный ресурс.  
Из особенностей этой строки можно отметить присутствие внутри символов +,=,/. Остается только подобрать нужный `Atom-based шифр`.  
На выходе имеем `https%3A%2F%2Fyadi.sk%2Fi%2FtlC89skL3S4V5z`, делаем url decode: `https://yadi.sk/i/tlC89skL3S4V5z`. 

По ссылке находится картинка (скрин из Microsoft Word).
Найти нужный шифр можно снова по [ссылке](http://itsecwiki.org/index.php/String_crypto) или перебором. Это стандартный шрифт Windows с названием `Wingdings`.   
Далее строку придется просто подобрать.
Получаем: `Qf0Gn4SmQfPJTIkF+414SiJqf4/mno6DKkwmnh6sbtT5Qf02LyA4KmT6`.  

Снова отмечаем характерные `Atom-based шифрам` признаки. Это оказывается сам `Atom128`.
Декодируем: `}r37$4m_n0c1m0N07pyRc_7pYrC3d{ftccse`

Уже видим флаг! Но мы все же привыли читать слева направо, производим `reverse text`:   `escctf{d3CrYp7_cRyp70N0m1c0n_m4$73r}`.    

Цепочка преобразований выглядит следующим образом:  
`Aztec Code -> base64 -> hex -> MEGAN-35 -> url ->  picture  -> font Windings -> Atom128 -> Reverse text`.  
Общее количество преобразований - 9, на что имеется бесполезный намек в названии таска.  

Congrats!

*Флаг: escctf{d3CrYp7_cRyp70N0m1c0n_m4$73r}*