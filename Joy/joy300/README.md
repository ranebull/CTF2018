# A novel about how in the third millennium humanity was captured by robots.
## Описание
Давай, пошли и вышли, приключение на 20 минут... [@Bot_inspection](https://telegram.me/ESCAPE_CTF_BOT)

Формат флага: escctf{answer}

Автор: [@KusBasLac](https://t.me/KusBasLac)
