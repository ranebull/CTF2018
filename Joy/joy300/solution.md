# Решение

Находишь бота на GitHub, находишь в файлах флаг. Ну или проходишь викторину.    
    
    0)linux 
	1)pwd 
	2)wine 
	3)chmod 
	4)ab ac b1 b2 b3 ed ee ef eg c 
	5)fi 
	6)dmesg 
	7)vim 
	8)grub 
	9)cd 
	10)iptables    
    
*Флаг: escctf{Dud3_1s_CTFch1k}* 
