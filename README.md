# EscapeCTF
## Категории тасков:
- Admin
- Web
- Stego
- Crypto
- Reverse
- Joy
- Recon/OSINT
- PPC

## Разработчики тасков и их категории:
- Трифанов А.В. (reverse, ppc)
- Мельников В.Г. (admin)
- Камышев С.В. (admin, crypto)
- Лебедев Д.А. (web)
- Иконников М.А. (web)
- Гребень А.Е. (crypto, stegano)
- Пешков Д.Е. (joy, recon)
- Рипп Д.А. (joy, recon)
- Кульбякина Н.Д. (joy, recon)

## Выбор платформы для соревнований:
- TinyCTF from [balidani](https://github.com/balidani/tinyctf-platform);
- TinyCTF from [huner2](https://github.com/huner2/Tiny-CTF-Platform);
- [PicoCTF](https://github.com/picoCTF/picoCTF);
- [CTFd](https://github.com/abdesslem/CTF).

## Формат флага:
escctf{some_text}