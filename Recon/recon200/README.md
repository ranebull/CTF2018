# A very famous person
## Описание

Сегодня на уроке поиска информации дали [такое](https://vk.com/realfamousperson) домашнее задание,где нужно найти какую-то информацию о странице. Преподаватель сказал,что даже не нужно выходить за пределы этого сайта,так что должно быть просто.

Формат флага: escctf{some_text}

Автор: [@ChipotleSouthwest](https://telegram.me/chipotlesouthwest)