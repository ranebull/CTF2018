# Решение

Задание можно решить несколькими способами.
Запустить strings и найти нужную строку "THat_wAs_eAsy_rIgHts?_To0_easy_ActUally_It\'s_A_tRap_wUhAhAhA." и оформить её согласно формату флага.

В IDA тоже самое можно увидеть через просмотр строк (shift+f12 по умолчанию).

Если смотреть код, то также легко заметить происходящие в нём операции:

	string check = "escctf";
	string a = "{";
	string b = "}";
	string f = "THat_wAs_eAsy_rIgHts?_To0_easy_ActUally_It's_A_tRap_wUhAhAhA.";

	string flag;
	cout << "Enter the flag:\n";
	cin >>flag;
	

	if (!flag.compare(check+a+f+b))
	{
		cout << "Congratilations! You have found the flag!!!\n";
	}
	else
	{
		cout << "Nope. Wrong flag. You just have mistyped right?\n";
	}

	return 0;

Флаг складывается из строк "escctf", "{", "THat_wAs_eAsy_rIgHts?_To0_easy_ActUally_It\'s_A_tRap_wUhAhAhA." и "}" и сравнивается с тем, что ввёл пользователь.

*Флаг: escctf{THat_wAs_eAsy_rIgHts?_To0_easy_ActUally_It's_A_tRap_wUhAhAhA.}*