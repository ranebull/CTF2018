# Решение
Каждый цвет имеет своё шестнадцатиричное представление. Оно в некоторых случаях имеет своё название:
- [List of colors: A-F](https://en.wikipedia.org/wiki/List_of_colors:_A–F)
- [List of colors: G-M](https://en.wikipedia.org/wiki/List_of_colors:_G–M)
- [List of colors: N-Z](https://en.wikipedia.org/wiki/List_of_colors:_N–Z)

Решением является первая буква каждого цвета, из которого составляется флаг.

*Флаг: escctf{creative}*