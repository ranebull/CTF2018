Тут все не так сложно.
Видим что у нас есть 8 квадратов. 8 Квадратов - 8 бит.

Последовательность под картинкой - порядок каналов в котором необходимо смотеть на кртинку.
Качем Stegsolve
Черный кватрат - 1, белый - 0

Получаем последовательность бинариев

01001000 
00110011 
01001100 
01101100 
00110000 
01011111 
01110111 
00110000 
01010010 
00110001 
01000100 

Переводим в ASCII любым конвертером - H3Ll0_w0R1D

Для простоты:
01001000 -H -B5
00110011 -3 -R3
01001100 -L -G4
01101100 -l -G6
00110000 -0 -R2
01011111 -_ -B4
01110111 -w -B3
00110000 -0 -R4
01010010 -R -G5
00110001 -1 -B2
01000100 -D -G2

(Знали бы вы как весело было делать это при помощи Paint и калькулятора...)

Флаг: escctf{H3Ll0_w0R1D}