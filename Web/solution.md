## web 100

* Заходим в /robots.txt, находим там папку flag.
Заходим /flag и получаем флаг

flag = escctf{r0b0t5_e4sy}

## web 200
* Раcкомментируем css файл security.css и видим на странице флаг

* Можно не раскомментировать файл, а просто посмотреть его содержимое

flag = escctf{4lw4ys_0n_styl3}

## web 300
* Находим в коде 2 функции на js которые делают не понятно что. Можно разобраться что она делает, а можно просто в консоли браузера выполнить функцию просто на написав *getInfo();*

flag = escctf{1_l0v3_j4v4scr1pt}

## web 400
* Смотрим в cookie и видим там security:d3JpdGUgaW4gY29va2llcyBhZG1pbjogcGFzcw==

* Декодируем base64 и получаем 

```
write in cookies admin: pass
```

* Добовляем новую cookie и получаем флаг

flag = escctf{sw33t_f0r_l4t3r}

## web 500
* В заголовке запроса надо прописать свойство

* еврейский язык - иврит - he
* медик - латынь - la
* английский - en

```
Accept-Language: he,la,en
```

flag = escctf{l00k_4t_th3_t1tl3}

### Имеется один ложный флаг в файле readme.php